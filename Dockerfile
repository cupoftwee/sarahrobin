FROM perl
WORKDIR /opt/myapp
COPY . .
RUN cpanm install -n Mojolicious::Plugin::EmailMailer
RUN cpanm install -n Authen::SASL
RUN cpanm install -n DateTime
RUN cpanm install -n Dotenv
RUN cpanm --installdeps -n .
EXPOSE 3000
CMD ./app.pl prefork
