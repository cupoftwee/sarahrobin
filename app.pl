#!/usr/bin/env perl
use strict;
use warnings;

use Mojolicious::Lite;
use DateTime;
use Dotenv;
# use Dotenv -load;

plugin 'EmailMailer' => {
  from => $ENV{EMAIL_USERNAME},
  how  => 'smtp',
  howargs => {
    hosts => [ 'smtp.mailbox.org' ],
    ssl   => 'starttls',
    sasl_username => $ENV{EMAIL_USERNAME},
    sasl_password => $ENV{EMAIL_PASSWORD}
  }
};

# top page
get '/' => sub {
  my $self = shift;
  
  $self->render('index');
};

get '/contact' => sub {
  my $self = shift;
  
  $self->render('contact');
};

# TODO - Form field validation & sanitization
#Receive and process data
post '/contact' => sub {
  my $self = shift;

  # Date & Timestamp
  my $dt = DateTime->now;         # Stores current date and time as datetime object
  my $date = $dt->ymd;            # Retrieves date as a string in 'yyyy-mm-dd' format
  my $time = $dt->hms;            # Retrieves time as a string in 'hh:mm:ss' format
  my $timestamp = "$date $time";  # creates 'yyyy-mm-dd hh:mm:ss' string
  
  #Get parameters
  my $name = $self->param('name');
  my $message = $self->param('message');

  # TODO - better error handling
  # Display the top page if there is no message
  unless (length $message) {
    $self->render('index', error =>'Message is empty');
    return;
  }
  
  #Save to flash
  $self->flash(name => $name);
  $self->flash(timestamp => $timestamp);
  $self->flash(message => $message);
  
  # Redirect back to contact page
  $self->redirect_to('/contact');

  # Format mail
  my $subject = 'New Contact Form Submission from ' . $name;
  my $fromField = '<p><b>From:</b> ' . $name . '</p><br>';
  my $messageField = '<p><b>Message:</b> ' . $message . '</p><br>';
  my $timestampField = '<p><b>Sent At:</b> ' . $timestamp . '</p><br>';
  my $bodyMarkup = $fromField . $messageField . $timestampField;

  # Send the email out
  $self->send_mail(
    to         => $ENV{EMAIL_USERNAME},
    subject    => $subject,
    html       => $bodyMarkup
  );
};

app->start;