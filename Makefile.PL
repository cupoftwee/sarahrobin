use strict;
use warnings;

use ExtUtils::MakeMaker;

WriteMakefile(
  VERSION   => '0.01',
  PREREQ_PM => {
    'Mojolicious' => '9.28',
    'Mojolicious::Plugin::EmailMailer' => '0.03',
    'Authen::SASL' => '2.16',
    'DateTime' => '1.59',
    'Dotenv' => '0.002'
  },
  test => {TESTS => 't/*.t'}
);
