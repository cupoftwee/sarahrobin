# Portfolio Rebuild

To start, run: `docker run --rm -d -p3000:3000 sarahrobin-2022`
* To stop, run: 
	* `docker stats` - get container PID
	* `docker stop <PID>` 
* to start local web dev server: `morbo ./myapp.pl`
* To deploy: 
	* Push up changes to main branch of https://gitlab.com/cupoftwee/sarahrobin repo. DO will automatically build & deploy docker image to https://sarahrobin-2022-st4mz.ondigitalocean.app/

## font?
	https://input.djr.com/


## ToDo
* Markdown content
* Asset handling
* PostCSS
* [x] Contact Form
* perf test after some content is ported over - lighthouse score vs current site